# OnDemand Template

This repo contains a generic template that can be used to easily deploy OnDemand applications!

## Things TODO

This is an 'all in one' template, meaning that it will self configure itself best it can. Maybe we should have multiple template repos instead? The two types I currently see are apps that utilize VNC, and apps that start a server of some kind.

I also need to test if my yaml syntax is correct, along with creating an app with this template to see if
OnDemand is ok with it.

## What is OnDemand?

OnDemand is a service that allows for arbitrary apps to be deployed on our HPCC.
It describes the attributes your app needs to be configured, how your app should be started,
and it will use this info to generate an easy to use front end that our
end users can utilize to configure and start your app.

OnDemand abstracts away the dirty details and offers an easy to use system for our end users.
It also make maintenance very simple, as most of the work consists of altering configuration
files and minor parts of start scripts. (Usually) no major software development work is necessary!

## Included Files

This repo contains some files that need to be altered. All sections that need to be altered will be enclosed in `![]!`, with a minor description in the middle. These sections will usually be comments. You should delete this section after you have implemented the proper information.

- `manifest.yml` - Contains high-level information about your app
- `form.yml` - Contains all attributes your application will use, along with rules for how these values are represented and shown on the front end.
- `form.js` - Allows for dynamic behavior and logic for user selection
- `view.html.erb` - Final HTML file that gets served to your user after the app is started - Be sure to delete if your app is VNC!
- `index.json` - A manifest of all files to be checked by the check script
- `template/script.sh.erb` - Start script for your app - Still working on...
- `template/before.sh.erb` - Script that runs before your app is started - Still working on...
- `template/after.sh.erb` - Script that runs after your app is started - Still working on...

We also provide some optional files:

- `icon.png` - Change this to your desired icon!
